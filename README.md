# Compiling 

```
make module
```

This will generate and SO that is a dynamic object, in other words a dynamic library for *nix

# SSHD config

The `sshd_config.example` file includes an example of the needed configuration to use this module. The sshd config file usually lives at `/etc/ssh/sshd_config`

Example:

```
UsePAM yes
Match User git
  AuthorizedKeysCommand /opt/gitlab/embedded/service/gitlab-shell/bin/gitlab-shell-authorized-keys-check git %u %k
  AuthorizedKeysCommandUser git
  AuthenticationMethods publickey,keyboard-interactive
  PasswordAuthentication no
  PubKeyAuthentication yes
Match all

```


# PAM config

On the previous configuration we enabled pam for login.
And by adding to `/etc/pam.d/sshd`

```
auth required SO_PATH
```

Or to `/etc/pam.conf` (if it is what your system uses)

```
sshd auth required SO_PATH
```

and `SO_PATH` is the compiled module path
