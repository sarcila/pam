package main

// AuthResult is the result of the authenticate function.
type AuthResult int

const (
	// AuthError is a failure.
	AuthError AuthResult = iota
	// AuthSuccess is a success.
	AuthSuccess
)

func authenticate(otp string) AuthResult {
	// if Verify(otp) {
	// 	return AuthSuccess
	// }
	return AuthError
}

func main() {
}
